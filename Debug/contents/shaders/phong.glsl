struct SVertexInput
{
  vec3 Position;
  vec2 TexCoord;
  vec3 Normal;
  ivec4 JointIds;
  vec4 JointWeights;
};

interface IVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SBaseLight
{
  vec3 Color;
  float AmbientIntensity;
  float DiffuseIntensity;
};

struct SDirectionalLight
{
  SBaseLight Base;
  vec3 Direction;
};

uniform mat4 WVP;
uniform mat4 WorldTransform;

uniform SDirectionalLight DirectionalLight;
uniform vec3 CameraPosition;
uniform float SpecularIntensity;  // specular reflectivity coefficient
uniform float SpecularPower;  // shininess or phong exponent

uniform bool TextureEnabled;
uniform sampler2D Texture;

shader vertexMain(in SVertexInput input : 0, out IVertexOutput output)
{
  vec4 position = vec4(input.Position, 1);
  vec4 normal = vec4(input.Normal, 0);
  
  gl_Position = WVP * position;
  output.TexCoord = input.TexCoord;
  output.Normal = (WorldTransform * normal).xyz;
  output.WorldPos = (WorldTransform * position).xyz;
}

vec4 computeLight(SBaseLight light, vec3 lightDirection, SVertexOutput input)
{
  vec4 ambientColor = vec4(light.Color, 1.0f) * light.AmbientIntensity;
  float diffuseFactor = dot(input.Normal, -lightDirection);

  vec4 diffuseColor  = vec4(0, 0, 0, 0);
  vec4 specularColor = vec4(0, 0, 0, 0);

  if (diffuseFactor > 0)
  {
    diffuseColor = vec4(light.Color, 1.0f) * light.DiffuseIntensity * diffuseFactor;

    vec3 vertexToEye = normalize(CameraPosition - input.WorldPos);
    vec3 lightReflect = normalize(reflect(lightDirection, input.Normal));
    float specularFactor = dot(vertexToEye, lightReflect);
    specularFactor = pow(specularFactor, SpecularPower);
    if (specularFactor > 0) {
      specularColor = vec4(light.Color, 1.0f) *
              SpecularIntensity * specularFactor;
    }
  }

  return (ambientColor + diffuseColor + specularColor);
}

vec4 computeDirectionalLight(SVertexOutput input)
{
  return computeLight(DirectionalLight.Base, DirectionalLight.Direction, input);
}

shader fragmentMain(in IVertexOutput iInput, out vec4 FragColor)
{
  SVertexOutput input;
  input.TexCoord = iInput.TexCoord;
  input.Normal = normalize(iInput.Normal);
  input.WorldPos = iInput.WorldPos;
  
  vec4 totalLight = computeDirectionalLight(input);
  
  if (TextureEnabled)
    FragColor = texture(Texture, input.TexCoord.xy) * totalLight;
  else
    FragColor = totalLight;
}

program Phong
{
  vs(410) = vertexMain();
  fs(410) = fragmentMain();
};
