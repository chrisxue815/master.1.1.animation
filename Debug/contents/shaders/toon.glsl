struct SVertexInput
{
  vec3 Position;
  vec2 TexCoord;
  vec3 Normal;
  ivec4 JointIds;
  vec4 JointWeights;
};

interface IVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SBaseLight
{
  vec3 Color;
  float AmbientIntensity;
  float DiffuseIntensity;
};

struct SDirectionalLight
{
  SBaseLight Base;
  vec3 Direction;
};

uniform mat4 WVP;
uniform mat4 WorldTransform;

uniform SDirectionalLight DirectionalLight;

uniform bool TextureEnabled;
uniform sampler2D Texture;

shader vertexMain(in SVertexInput input : 0, out IVertexOutput output)
{
  vec4 position = vec4(input.Position, 1);
  vec4 normal = vec4(input.Normal, 0);
  
  gl_Position = WVP * position;
  output.TexCoord = input.TexCoord;
  output.Normal = normalize((WorldTransform * normal).xyz);
  output.WorldPos = (WorldTransform * position).xyz;
}

shader fragmentMain(in IVertexOutput input, out vec4 FragColor)
{
  float intensity = dot(-DirectionalLight.Direction, input.Normal);
  vec4 color;
  
  if (intensity > 0.75)
    color = vec4(1.0, 0.5, 0.5, 1.0);
  else if (intensity > 0.5)
    color = vec4(0.6, 0.3, 0.3, 1.0);
  else if (intensity > 0.25)
    color = vec4(0.4, 0.2, 0.2, 1.0);
  else
    color = vec4(0.2, 0.1, 0.1, 1.0);
  
  if (TextureEnabled)
    FragColor = texture(Texture, input.TexCoord.xy) * color;
  else
    FragColor = color;
}

program Toon
{
  vs(410) = vertexMain();
  fs(410) = fragmentMain();
};
