const float PI = 3.14159265358979323846264;

struct SVertexInput
{
  vec3 Position;
  vec2 TexCoord;
  vec3 Normal;
  ivec4 JointIds;
  vec4 JointWeights;
};

interface IVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SBaseLight
{
  vec3 Color;
  float AmbientIntensity;
  float DiffuseIntensity;
};

struct SDirectionalLight
{
  SBaseLight Base;
  vec3 Direction;
};

uniform mat4 WVP;
uniform mat4 WorldTransform;

uniform SDirectionalLight DirectionalLight;
uniform vec3 CameraPosition;
uniform float SpecularIntensity;
uniform float SpecularPower;

uniform float Albedo;
uniform float Roughness;

uniform bool TextureEnabled;
uniform sampler2D Texture;

shader vertexMain(in SVertexInput input : 0, out IVertexOutput output)
{
  vec4 position = vec4(input.Position, 1);
  vec4 normal = vec4(input.Normal, 0);
  
  gl_Position = WVP * position;
  output.TexCoord = input.TexCoord;
  output.Normal = normalize((WorldTransform * normal).xyz);
  output.WorldPos = (WorldTransform * position).xyz;
}

vec4 computeDiffuse(SBaseLight light, vec3 lightDirection, SVertexOutput input)
{
  float roughSqr = Roughness * Roughness;
  
  vec3 incoming = -lightDirection;
  vec3 outcoming = normalize(CameraPosition - input.WorldPos);
  
  float cosIn = dot(input.Normal, incoming);
  
  vec3 azimuthIn = cross(cross(input.Normal, incoming), input.Normal);
  vec3 azimuthOut = cross(cross(input.Normal, outcoming), input.Normal);
  
  float cosDiff = dot(azimuthIn, azimuthOut);
  
  float coef = 0;
  
  if (cosDiff > 0)
  {
    float cosOut = dot(input.Normal, outcoming);
    
    float sinAlpha, tanBeta;
    
    if (cosIn > cosOut)
    {
      // in < out, alpha is out, beta is in
      sinAlpha = sqrt(1 - cosOut * cosOut);
      tanBeta = sqrt(1 - cosIn * cosIn) / cosIn;
    }
    else
    {
      sinAlpha = sqrt(1 - cosIn * cosIn);
      tanBeta = sqrt(1 - cosOut * cosOut) / cosOut;
    }
    
    float b = 0.45 * roughSqr / (roughSqr + 0.09);
    coef = b * cosDiff * sinAlpha * tanBeta;
  }

  float a = 1 - 0.5 * roughSqr / (roughSqr + 0.33);
  
  vec4 lightReflect = Albedo / PI * cosIn * (a + coef) * vec4(light.Color, 1.0f);
  
  return lightReflect;
}

vec4 computeLight(SBaseLight light, vec3 lightDirection, SVertexOutput input)
{
  vec4 diffuseColor = computeDiffuse(light, lightDirection, input);
  return diffuseColor;
}

vec4 computeDirectionalLight(SVertexOutput input)
{
  return computeLight(DirectionalLight.Base, DirectionalLight.Direction, input);
}

shader fragmentMain(in IVertexOutput iInput, out vec4 FragColor)
{
  SVertexOutput input;
  input.TexCoord = iInput.TexCoord;
  input.Normal = normalize(iInput.Normal);
  input.WorldPos = iInput.WorldPos;
  
  vec4 totalLight = computeDirectionalLight(input);
  
  if (TextureEnabled)
    FragColor = texture(Texture, input.TexCoord.xy) * totalLight;
  else
    FragColor = totalLight;
}

program OrenNayar
{
  vs(410) = vertexMain();
  fs(410) = fragmentMain();
};
