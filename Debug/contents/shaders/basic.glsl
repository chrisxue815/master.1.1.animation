struct SVertexInput
{
  vec3 Position;
  vec4 Color;
};

interface IVertexOutput
{
  vec4 Color;
};

uniform mat4 WVP;

shader vertexMain(in SVertexInput input : 0, out IVertexOutput output)
{
  gl_Position = WVP * vec4(input.Position, 1.0);
  output.Color = input.Color;
}

shader fragmentMain(in IVertexOutput iInput, out vec4 FragColor)
{
  FragColor = vec4(0, 0, 0, 1);
}

program Basic
{
  vs(410) = vertexMain();
  fs(410) = fragmentMain();
};
