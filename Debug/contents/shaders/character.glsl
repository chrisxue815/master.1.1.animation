struct SVertexInput
{
  vec3 Position;
  vec2 TexCoord;
  vec3 Normal;
  ivec4 JointIds;
  vec4 JointWeights;
};

interface IVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SVertexOutput
{
  vec2 TexCoord;
  vec3 Normal;
  vec3 WorldPos;
};

struct SBaseLight
{
  vec3 Color;
  float AmbientIntensity;
  float DiffuseIntensity;
};

struct SDirectionalLight
{
  SBaseLight Base;
  vec3 Direction;
};

struct SAttenuation
{
  float Constant;
  float Linear;
  float Exp;
};

struct SPointLight
{
  SBaseLight Base;
  vec3 Position;
  SAttenuation Atten;
};

struct SSpotLight
{
  SPointLight Base;
  vec3 Direction;
  float Cutoff;
};

const int MaxNumJoints = 100;

const int MaxNumPointLights = 2;
const int MaxNumSpotLights = 2;

uniform mat4 WVP;
uniform mat4 World;
uniform mat4 Joints[MaxNumJoints];

uniform int NumPointLights;
uniform int NumSpotLights;
uniform SDirectionalLight DirectionalLight;
uniform SPointLight PointLights[MaxNumPointLights];
uniform SSpotLight SpotLights[MaxNumSpotLights];
uniform sampler2D ColorMap;
uniform vec3 CameraPosition;
uniform float MatSpecularIntensity;
uniform float SpecularPower;

shader vertexMain(in SVertexInput input : 0, out IVertexOutput output)
{
  mat4 jointTransform = Joints[input.JointIds[0]] * input.JointWeights[0];
  jointTransform += Joints[input.JointIds[1]] * input.JointWeights[1];
  jointTransform += Joints[input.JointIds[2]] * input.JointWeights[2];
  jointTransform += Joints[input.JointIds[3]] * input.JointWeights[3];

  vec4 position = jointTransform * vec4(input.Position, 1.0);
  gl_Position = WVP * position;
  output.TexCoord = input.TexCoord;
  vec4 normal = jointTransform * vec4(input.Normal, 0.0);
  output.Normal = (World * normal).xyz;
  output.WorldPos = (World * position).xyz;
}

vec4 calcLightInternal(SBaseLight light, vec3 lightDirection, SVertexOutput input)
{
  vec4 ambientColor = vec4(light.Color, 1.0f) * light.AmbientIntensity;
  float diffuseFactor = dot(input.Normal, -lightDirection);

  vec4 diffuseColor  = vec4(0, 0, 0, 0);
  vec4 specularColor = vec4(0, 0, 0, 0);

  if (diffuseFactor > 0)
  {
    diffuseColor = vec4(light.Color, 1.0f) * light.DiffuseIntensity * diffuseFactor;

    vec3 vertexToEye = normalize(CameraPosition - input.WorldPos);
    vec3 lightReflect = normalize(reflect(lightDirection, input.Normal));
    float specularFactor = dot(vertexToEye, lightReflect);
    specularFactor = pow(specularFactor, SpecularPower);
    if (specularFactor > 0) {
      specularColor = vec4(light.Color, 1.0f) *
              MatSpecularIntensity * specularFactor;
    }
  }

  return (ambientColor + diffuseColor + specularColor);
}

vec4 calcDirectionalLight(SVertexOutput input)
{
  return calcLightInternal(DirectionalLight.Base, DirectionalLight.Direction, input);
}

vec4 calcPointLight(SPointLight l, SVertexOutput input)
{
  vec3 lightDirection = input.WorldPos - l.Position;
  float Distance = length(lightDirection);
  lightDirection = normalize(lightDirection);

  vec4 Color = calcLightInternal(l.Base, lightDirection, input);
  float SAttenuation =  l.Atten.Constant +
             l.Atten.Linear * Distance +
             l.Atten.Exp * Distance * Distance;

  return Color / SAttenuation;
}

vec4 calcSpotLight(SSpotLight l, SVertexOutput input)
{
  vec3 LightToPixel = normalize(input.WorldPos - l.Base.Position);
  float SpotFactor = dot(LightToPixel, l.Direction);

  if (SpotFactor > l.Cutoff) {
    vec4 Color = calcPointLight(l.Base, input);
    return Color * (1.0 - (1.0 - SpotFactor) * 1.0/(1.0 - l.Cutoff));
  }
  else {
    return vec4(0,0,0,0);
  }
}

shader fragmentMain(in IVertexOutput iInput, out vec4 FragColor)
{
  SVertexOutput input;
  input.TexCoord = iInput.TexCoord;
  input.Normal = normalize(iInput.Normal);
  input.WorldPos = iInput.WorldPos;

  vec4 TotalLight = calcDirectionalLight(input);

  for (int i = 0 ; i < NumPointLights ; i++) {
    TotalLight += calcPointLight(PointLights[i], input);
  }

  for (int i = 0 ; i < NumSpotLights ; i++) {
    TotalLight += calcSpotLight(SpotLights[i], input);
  }

  FragColor = texture(ColorMap, input.TexCoord.xy) * TotalLight;
}

program Character
{
  vs(410) = vertexMain();
  fs(410) = fragmentMain();
};
