#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <assimp/scene.h>

namespace chrisxue
{
    class JointTransform
    {
    public:
        JointTransform();

        static glm::vec3 interpolateScaling(float time, const aiNodeAnim *nodeAnim);
        static glm::quat interpolateRotation(float time, const aiNodeAnim *nodeAnim);
        static glm::vec3 interpolatePosition(float time, const aiNodeAnim *nodeAnim);    
        static unsigned int findScaling(float time, const aiNodeAnim *nodeAnim);
        static unsigned int findRotation(float time, const aiNodeAnim *nodeAnim);
        static unsigned int findPosition(float time, const aiNodeAnim *nodeAnim);

        glm::vec3 Scaling;
        glm::quat Rotation;
        glm::vec3 Translation;
        glm::mat4 ToJointSpace;
        glm::mat4 FinalTransform;
    };

    class SkeletonTransform
    {

    };
}
