#pragma once

#include "Effect.h"

#include <assimp/scene.h>

#include "Light.h"
#include "Extensions.h"

namespace chrisxue
{
    class CharacterEffect : public Effect
    {
    public:
        static const unsigned MaxNumPointLights = 2;
        static const unsigned MaxNumSpotLights = 2;
        static const unsigned MaxNumJoints = 100;

        CharacterEffect();

        virtual void init();

        void setWVP(const glm::mat4 &wvp);
        void setWorldMatrix(const glm::mat4 &world);
        void setColorTextureUnit(unsigned int textureUnit);
        void setDirectionalLight(const DirectionalLight &light);
        void setPointLights(unsigned int numLights, const PointLight *lights);
        void setSpotLights(unsigned int numLights, const SpotLight *lights);
        void setCameraPosition(const glm::vec3 &cameraPosition);
        void setMatSpecularIntensity(float intensity);
        void setMatSpecularPower(float power);
        void setJointTransform(unsigned int index, const glm::mat4 &transform);

    private:
        GLuint WVPLocation;
        GLuint WorldMatrixLocation;
        GLuint ColorTextureLocation;
        GLuint CameraPositionLocation;
        GLuint MatSpecularIntensityLocation;
        GLuint MatSpecularPowerLocation;
        GLuint NumPointLightsLocation;
        GLuint NumSpotLightsLocation;

        struct
        {
            GLuint Color;
            GLuint AmbientIntensity;
            GLuint DiffuseIntensity;
            GLuint Direction;
        } DirLightLocation;

        struct
        {
            GLuint Color;
            GLuint AmbientIntensity;
            GLuint DiffuseIntensity;
            GLuint Position;
            struct
            {
                GLuint Constant;
                GLuint Linear;
                GLuint Exp;
            } Atten;
        } PointLightsLocation[MaxNumPointLights];

        struct
        {
            GLuint Color;
            GLuint AmbientIntensity;
            GLuint DiffuseIntensity;
            GLuint Position;
            GLuint Direction;
            GLuint Cutoff;
            struct
            {
                GLuint Constant;
                GLuint Linear;
                GLuint Exp;
            } Atten;
        } SpotLightsLocation[MaxNumSpotLights];

        GLuint JointLocation[MaxNumJoints];
    };
}
