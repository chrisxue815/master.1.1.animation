#pragma once

#include "Effect.h"

#include <glm/glm.hpp>

#include "Light.h"

namespace chrisxue
{
	class ToonEffect : public Effect
	{
	public:
		ToonEffect(void);
		~ToonEffect(void);

		//override
		void init();
		void update();
		void draw();

        void setWVP(const glm::mat4 &wvp);
        void setWorldTransform(const glm::mat4 &worldTransform);
        void setDirectionalLight(const DirectionalLight &light);
        void setTextureEnabled(bool textureEnabled);
        void setTexture(unsigned int texture);

    private:
        GLuint WVPLocation;
        GLuint WorldTransformLocation;
        GLuint TextureEnabledLocation;
        GLuint TextureLocation;

        struct
        {
            GLuint Color;
            GLuint AmbientIntensity;
            GLuint DiffuseIntensity;
            GLuint Direction;
        } DirLightLocation;
	};
}
