#include "GridLineGround.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "BasicEffect.h"
#include "Game.h"
#include "Camera.h"

using namespace chrisxue;
using namespace glm;

GridLineGround::GridLineGround(void)
{
    effect = new BasicEffect();
}

GridLineGround::~GridLineGround(void)
{
    delete effect;
}

void GridLineGround::init()
{
    vec3 *p = new vec3[NumPoints];
    int i = 0;

    for (int row = -Range; row <= Range; row += TileWidth)
    {
        p[i++] = vec3(-Range, 0, row);
        p[i++] = vec3(Range, 0, row);
    }

    for (int col = -Range; col <= Range; col += TileWidth)
    {
        p[i++] = vec3(col, 0, -Range);
        p[i++] = vec3(col, 0, Range);
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(p[0]) * NumPoints, p, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
}

void GridLineGround::draw()
{
    effect->use();
    glBindVertexArray(vao);

    effect->setWVP(game->camera->VP());

    glDrawArrays(GL_LINES, 0, NumPoints);

    glBindVertexArray(0);
    effect->unuse();
}
