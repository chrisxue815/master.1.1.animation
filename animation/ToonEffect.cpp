#include "ToonEffect.h"

#include <gl/glfx.h>

#include <glm/gtc/type_ptr.hpp>

#include "Extensions.h"

using namespace chrisxue;
using namespace glm;

static const char *EffectFile = "contents/shaders/toon.glsl";

ToonEffect::ToonEffect(void) : Effect(EffectFile)
{
}

ToonEffect::~ToonEffect(void)
{
}

void ToonEffect::init(void)
{
	compileProgram("Toon");
	WVPLocation = getUniformLocation("WVP");
    WorldTransformLocation = getUniformLocation("WorldTransform");
    DirLightLocation.Color = getUniformLocation("DirectionalLight.Base.Color");
    DirLightLocation.AmbientIntensity = getUniformLocation("DirectionalLight.Base.AmbientIntensity");
    DirLightLocation.DiffuseIntensity = getUniformLocation("DirectionalLight.Base.DiffuseIntensity");
    DirLightLocation.Direction = getUniformLocation("DirectionalLight.Direction");
    TextureEnabledLocation = getUniformLocation("TextureEnabled");
    TextureLocation = getUniformLocation("Texture");

    use();

    DirectionalLight directionalLight;
    directionalLight.Color = vec3(1.0f, 1.0f, 1.0f);
    directionalLight.AmbientIntensity = 0.55f;
    directionalLight.DiffuseIntensity = 0.9f;
    directionalLight.Direction = vec3(-1.0f, -1.0f, -1.0f);
    setDirectionalLight(directionalLight);

    setTextureEnabled(false);
    setTexture(0);

    unuse();
}

void ToonEffect::setWVP(const mat4 &wvp)
{
    glUniformMatrix4fv(WVPLocation, 1, false, value_ptr(wvp));
}

void ToonEffect::setWorldTransform(const mat4 &worldTransform)
{
    glUniformMatrix4fv(WorldTransformLocation, 1, false, value_ptr(worldTransform));
}

void ToonEffect::setDirectionalLight(const DirectionalLight &light)
{
    glUniform3f(DirLightLocation.Color, light.Color.x, light.Color.y, light.Color.z);
    glUniform1f(DirLightLocation.AmbientIntensity, light.AmbientIntensity);
    vec3 Direction = light.Direction;
    Direction = safeNormalize(Direction);
    glUniform3f(DirLightLocation.Direction, Direction.x, Direction.y, Direction.z);
    glUniform1f(DirLightLocation.DiffuseIntensity, light.DiffuseIntensity);
}

void ToonEffect::setTextureEnabled(bool textureEnabled)
{
    glUniform1i(TextureEnabledLocation, textureEnabled);
}

void ToonEffect::setTexture(unsigned int texture)
{
    glUniform1i(TextureLocation, texture);
}
