#include "BasicEffect.h"

#include <glm/gtc/type_ptr.hpp>

using namespace chrisxue;
using namespace glm;

static const char *EffectFile = "contents/shaders/basic.glsl";

BasicEffect::BasicEffect(void) : Effect(EffectFile)
{
    compileProgram("Basic");

    WVPLocation = getUniformLocation("WVP");
}

BasicEffect::~BasicEffect(void)
{
}

void BasicEffect::setWVP(const glm::mat4 &wvp)
{
    glUniformMatrix4fv(WVPLocation, 1, false, value_ptr(wvp));
}
