#include "Character.h"

#include <iostream>

#define _USE_MATH_DEFINES
#include <cmath>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "Game.h"
#include "Camera.h"
#include "Model.h"
#include "CharacterEffect.h"
#include "JointTransform.h"
#include "SkeletonAnimation.h"

using namespace chrisxue;
using namespace glm;
using namespace std;

const float Character::InterpolationTime = 0.5f;

Character::Character()
{
    scaling = vec3(0.1f);
    rotation = quat(vec3(0, 0, 0));
    position = vec3(0, 0, 0);

    initialLooking = vec3(0, 0, 1);
    initialUp = vec3(0, 1, 0);

    originalLooking = looking();

    speed = 20;

    state = Idle1;
    previousState = Idle1;
    animationStartTime = 0;
    interpolationEndTime = 0;
}

Character::~Character()
{
    for (int i = 0; i < arraySize(model); i++) delete model[i];
    delete effect;
}

void Character::init(CharacterName name)
{
    switch (name)
    {
    case XinZhao:
        model[Idle1] = Model::load("contents/models/XinZhao/idle1.dae");
        model[Idle2] = Model::load("contents/models/XinZhao/idle2.dae");
        model[Run] = Model::load("contents/models/XinZhao/run.dae");
        model[Attack1] = Model::load("contents/models/XinZhao/attack1.dae");
        model[Dance] = Model::load("contents/models/XinZhao/dance.dae");
        break;
    case JarvanIV:
        model[Idle1] = Model::load("contents/models/JarvanIV/idle1.dae");
        model[Idle2] = Model::load("contents/models/JarvanIV/idle2.dae");
        model[Run] = Model::load("contents/models/JarvanIV/run.dae");
        model[Attack1] = Model::load("contents/models/JarvanIV/attack1.dae");
        model[Dance] = Model::load("contents/models/JarvanIV/dance.dae");
    }

    effect = new CharacterEffect();
    effect->init();

    Entity::init();
}

void Character::update()
{
    Entity::update();

    if (Target) updateTracking();

    previousState = state;
}

void Character::draw()
{
    effect->use();

    float animationTime = game->TotalSeconds - animationStartTime;

    SkeletonAnimation *animation = model[state]->Animation;

    if (game->TotalSeconds < interpolationEndTime)
    {
        // interpolate between 2 animations
        // this function is not working, a part of reason is that the skeletons of 2 animations
        // are different (probably optimised by Maya or OpenCollada)
        vector<JointTransform> t = animation->JointTransforms;
        float amount = animationTime / InterpolationTime;
        animation->interpolate(transform, nextTransform, amount, t);

        for (uint i = 0; i < t.size(); i++)
        {
            effect->setJointTransform(i, t[i].FinalTransform);
        }
    }
    else
    {
        // update animation
        const vector<JointTransform> &transform = animation->transformJoints(animationTime);

        for (uint i = 0; i < transform.size(); i++)
        {
            effect->setJointTransform(i, transform[i].FinalTransform);
        }
    }

    effect->setCameraPosition(game->camera->position);

    mat4 world = translate(position) * toMat4(rotation) * scale(scaling);
    mat4 wvp = game->camera->VP() * world;

    effect->setWVP(wvp);
    effect->setWorldMatrix(world);

    model[state]->draw();

    effect->unuse();
}

void Character::move()
{
    Entity::move();

    state = Run;
}

void Character::stop()
{
    Entity::stop();

    state = Idle1;

    //if (previousState != state) stateChanged();
}

void Character::stateChanged()
{
    animationStartTime = game->TotalSeconds;
    interpolationEndTime = animationStartTime + InterpolationTime;

    SkeletonAnimation *animation = model[state]->Animation;
    SkeletonAnimation *previousAnimation = model[previousState]->Animation;

    transform = previousAnimation->AnimatedNodes;
    animation->transformJoints(InterpolationTime);
    nextTransform = animation->AnimatedNodes;
}

void Character::updateTracking()
{
    vec3 toTarget = Target->position - position;
    float distance = length(toTarget);

    if (distance < 20)
    {
        if (Target->state == Attack1)
        {
            rotation = rotate(Target->initialLooking, -toTarget);
            move();
        }
    }
    else if (distance < 40)
    {
        if (!moving || distance > 39)
        {
            rotation = rotate(initialLooking, toTarget);
            stop();
            state = Attack1;
        }
    }
    else if (distance < 150)
    {
        rotation = rotate(initialLooking, toTarget);
        stop();
        state = Dance;
    }
    else
    {
        rotation = rotate(initialLooking, toTarget);
        stop();
        state = Idle1;
    }
}
