#include "PhongEffect.h"

#include <gl/glfx.h>

#include <glm/gtc/type_ptr.hpp>

#include "Extensions.h"

using namespace chrisxue;
using namespace glm;

static const char *EffectFile = "contents/shaders/phong.glsl";

PhongEffect::PhongEffect(void) : Effect(EffectFile)
{
}

PhongEffect::~PhongEffect(void)
{
}

void PhongEffect::init(void)
{
	compileProgram("Phong");
	WVPLocation = getUniformLocation("WVP");
    WorldTransformLocation = getUniformLocation("WorldTransform");
    DirLightLocation.Color = getUniformLocation("DirectionalLight.Base.Color");
    DirLightLocation.AmbientIntensity = getUniformLocation("DirectionalLight.Base.AmbientIntensity");
    DirLightLocation.Direction = getUniformLocation("DirectionalLight.Direction");
    DirLightLocation.DiffuseIntensity = getUniformLocation("DirectionalLight.Base.DiffuseIntensity");
    CameraPositionLocation = getUniformLocation("CameraPosition");
    SpecularIntensityLocation = getUniformLocation("SpecularIntensity");
    SpecularPowerLocation = getUniformLocation("SpecularPower");
    TextureEnabledLocation = getUniformLocation("TextureEnabled");
    TextureLocation = getUniformLocation("Texture");

    use();

    DirectionalLight directionalLight;
    directionalLight.Color = vec3(1.0f, 1.0f, 1.0f);
    directionalLight.AmbientIntensity = 0.55f;
    directionalLight.DiffuseIntensity = 0.9f;
    directionalLight.Direction = vec3(-1.0f, -1.0f, -1.0f);
    setDirectionalLight(directionalLight);

    setSpecularIntensity(1);
    setSpecularPower(100);

    setTextureEnabled(false);
    setTexture(0);

    unuse();
}

void PhongEffect::setWVP(const mat4 &wvp)
{
    glUniformMatrix4fv(WVPLocation, 1, false, value_ptr(wvp));
}

void PhongEffect::setWorldTransform(const mat4 &worldTransform)
{
    glUniformMatrix4fv(WorldTransformLocation, 1, false, value_ptr(worldTransform));
}

void PhongEffect::setDirectionalLight(const DirectionalLight &light)
{
    glUniform3f(DirLightLocation.Color, light.Color.x, light.Color.y, light.Color.z);
    glUniform1f(DirLightLocation.AmbientIntensity, light.AmbientIntensity);
    vec3 Direction = light.Direction;
    Direction = safeNormalize(Direction);
    glUniform3f(DirLightLocation.Direction, Direction.x, Direction.y, Direction.z);
    glUniform1f(DirLightLocation.DiffuseIntensity, light.DiffuseIntensity);
}

void PhongEffect::setCameraPosition(const glm::vec3 &cameraPosition)
{
    glUniform3f(CameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
}

void PhongEffect::setSpecularIntensity(float intensity)
{
    glUniform1f(SpecularIntensityLocation, intensity);
}

void PhongEffect::setSpecularPower(float power)
{
    glUniform1f(SpecularPowerLocation, power);
}

void PhongEffect::setTextureEnabled(bool textureEnabled)
{
    glUniform1i(TextureEnabledLocation, textureEnabled);
}

void PhongEffect::setTexture(unsigned int texture)
{
    glUniform1i(TextureLocation, texture);
}
