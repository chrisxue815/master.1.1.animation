#pragma once

namespace chrisxue
{
    class BasicGame
    {
    public:
        BasicGame();
        ~BasicGame();

        void exec(int *argc, char* *argv);

        virtual void init();

        virtual void update();
        virtual void draw();

        int Width;
        int Height;

        float ElapsedSeconds;
        float TotalSeconds;

        bool Running;

    private:
        void initOpenGL();

        static void loop_static();
        void loop();

        static BasicGame *Instance;

        float previousTotalSeconds;
    };
}
