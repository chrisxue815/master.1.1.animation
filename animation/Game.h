#pragma once

#include "BasicGame.h"

#include "Teapot.h"

namespace chrisxue
{
    class Camera;
    class GridLineGround;

    class Game : public BasicGame
    {
    public:
        Game();
        ~Game();

        // override
        void init();
        void update();
        void draw();

        void initObjects();

        static Game *Instance;

        Camera *camera;
        GridLineGround *ground;
        Teapot *teapots[NumTeapotEffectType];
    };
}
