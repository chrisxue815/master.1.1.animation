#include "Model.h"

#include <assimp/postprocess.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include "Extensions.h"
#include "Texture.h"
#include "JointTransform.h"
#include "SkeletonAnimation.h"

using namespace chrisxue;
using namespace glm;
using namespace Assimp;
using namespace std;

enum VertexInputParameter
{
    PositionParameter, TexcoordParameter, NormalParameter, JointIdsParameter, JointWeightsParameter
};

Model *Model::load(const char *modelPath)
{
    Importer *importer = new Importer();

    const aiScene *scene = importer->ReadFile(modelPath,
        aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

    if (!scene)
    {
        fprintf(stderr, "Error: '%s'\n", importer->GetErrorString());
        throw 1;
    }

    Model *model = new Model(importer, scene, modelPath);

    return model;
}

Model::Model(Importer *importer, const aiScene *scene, const char *modelPath)
{
    this->importer = importer;
    this->scene = scene;
    this->modelPath = string(modelPath);

    initBuffers();
}

Model::~Model()
{
    delete Animation;
    delete importer;
}

void Model::initBuffers()
{
    Animation = new SkeletonAnimation(this);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(arraySize(buffers), buffers);

    meshes.resize(scene->mNumMeshes);
    textures.resize(scene->mNumMaterials);

    uint numVertices = 0;
    uint numIndices = 0;

    for (uint i = 0; i < meshes.size(); i++)
    {
        meshes[i].MaterialIndex = scene->mMeshes[i]->mMaterialIndex;
        meshes[i].NumIndices = scene->mMeshes[i]->mNumFaces * 3;
        meshes[i].BaseVertex = numVertices;
        meshes[i].BaseIndex = numIndices;

        numVertices += scene->mMeshes[i]->mNumVertices;
        numIndices  += meshes[i].NumIndices;
    }

    // reserve space in the vectors for the vertex attributes and indices
    positions.reserve(numVertices);
    normals.reserve(numVertices);
    texcoords.reserve(numVertices);
    joints.resize(numVertices);
    indices.reserve(numIndices);

    // initialize the meshes in the scene one by one
    for (uint i = 0; i < meshes.size(); i++)
    {
        const aiMesh *mesh = scene->mMeshes[i];
        initMesh(i, mesh);
    }

    initMaterials();

    // generate and populate the buffers with vertex attributes and the indices
    glBindBuffer(GL_ARRAY_BUFFER, buffers[PositionBuffer]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(PositionParameter);
    glVertexAttribPointer(PositionParameter, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[TexcoordBuffer]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(texcoords[0]) * texcoords.size(), &texcoords[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(TexcoordParameter);
    glVertexAttribPointer(TexcoordParameter, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[NormalBuffer]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * normals.size(), &normals[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(NormalParameter);
    glVertexAttribPointer(NormalParameter, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[JointBuffer]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(joints[0]) * joints.size(), &joints[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(JointIdsParameter);
    glVertexAttribIPointer(JointIdsParameter, 4, GL_INT, sizeof(VertexJointData), (const GLvoid*)0);
    glEnableVertexAttribArray(JointWeightsParameter);
    glVertexAttribPointer(JointWeightsParameter, 4, GL_FLOAT, GL_FALSE, sizeof(VertexJointData), (const GLvoid*)16);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[IndexBuffer]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

    if (glGetError() != GL_NO_ERROR)
    {
        throw 1;
    }

    glBindVertexArray(0);

    positions.clear();
    normals.clear();
    texcoords.clear();
    joints.clear();
    indices.clear();
}

void Model::initMesh(uint meshIndex, const aiMesh *mesh)
{
    // populate the vertex attribute vectors
    for (uint i = 0; i < mesh->mNumVertices; i++)
    {
        vec3 pos = toGlmVec3(mesh->mVertices[i]);
        vec3 normal = toGlmVec3(mesh->mNormals[i]);
        vec2 texcoord = mesh->HasTextureCoords(0) ? toGlmVec2(mesh->mTextureCoords[0][i]) : vec2();

        positions.push_back(pos);
        normals.push_back(normal);
        texcoords.push_back(texcoord);
    }

    Animation->loadJoints(meshIndex, mesh, joints);

    // populate the index buffer
    for (uint i = 0; i < mesh->mNumFaces; i++)
    {
        const aiFace &face = mesh->mFaces[i];
        assert(face.mNumIndices == 3);
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }
}

void Model::initMaterials()
{
    // extract the directory part from the file name
    string::size_type slashIndex = modelPath.find_last_of("/");
    string modelDir;

    if (slashIndex == string::npos)
    {
        modelDir = ".";
    }
    else if (slashIndex == 0)
    {
        modelDir = "/";
    }
    else
    {
        modelDir = modelPath.substr(0, slashIndex);
    }

    // initialize the materials
    for (uint i = 0; i < scene->mNumMaterials; i++)
    {
        const aiMaterial *material = scene->mMaterials[i];
        textures[i] = NULL;

        if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {
            aiString texturePathAI;

            if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePathAI, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                string texturePath(texturePathAI.data);

                string firstTwoChar = texturePath.substr(0, 2);
                if (firstTwoChar == ".\\" || firstTwoChar == "./")
                {
                    texturePath = texturePath.substr(2, texturePath.size() - 2);
                }

                string fullTexturePath = modelDir + "/" + texturePath;

                Texture *texture = Texture::load(GL_TEXTURE_2D, fullTexturePath);

                textures[i] = texture;
                printf("%d - loaded texture '%s'\n", i, fullTexturePath.c_str());
            }
        }
    }
}

void Model::draw(GLenum textureUnit)
{
    glBindVertexArray(vao);

    for (uint i = 0; i < meshes.size(); i++)
    {
        const uint MaterialIndex = meshes[i].MaterialIndex;

        if (MaterialIndex >= textures.size()) throw 1;

        if (textures[MaterialIndex])
        {
            textures[MaterialIndex]->bind(textureUnit);
        }

        glDrawElementsBaseVertex(GL_TRIANGLES,
            meshes[i].NumIndices,
            GL_UNSIGNED_INT,
            (void*)(sizeof(uint) * meshes[i].BaseIndex),
            meshes[i].BaseVertex);
    }

    glBindVertexArray(0);
}

void Model::draw()
{
    draw(GL_TEXTURE0);
}

void VertexJointData::addJointData(uint jointId, float weight)
{
    for (uint i = 0; i < arraySize(JointIds); i++)
    {
        if (Weights[i] == 0.0)
        {
            JointIds[i] = jointId;
            Weights[i] = weight;
            return;
        }
    }

    throw 1;
}
