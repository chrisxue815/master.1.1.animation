#include "BasicGame.h"

#include <iostream>

#include <GL/glew.h>
#include <GL/wglew.h>
#include <GL/freeglut.h>

using namespace chrisxue;

BasicGame *BasicGame::Instance = NULL;

BasicGame::BasicGame()
{
    Instance = this;
    Width = 800;
    Height = 600;
    ElapsedSeconds = 0;
    TotalSeconds = 0;
    Running = true;
    previousTotalSeconds = 0;
}

BasicGame::~BasicGame()
{
    if (Instance == this) Instance = NULL;
}

void BasicGame::exec(int *argc, char* *argv)
{
    // Set up the window
    glutInit(argc, argv);

    initOpenGL();

    init();

    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

    // Begin infinite event loop
    glutMainLoop();
}

void BasicGame::initOpenGL()
{
    // Set up the window
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);
    glutInitWindowSize(Width, Height);
    glutCreateWindow("Game");

    // Tell glut where the display function is
    glutDisplayFunc(loop_static);
    glutIdleFunc(loop_static);

    // A call to glewInit() must be done after glut is initialized!
    GLenum res = glewInit();

    // Check for any errors
    if (res != GLEW_OK) {
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        throw 1;
    }

    glEnable(GL_DEPTH_TEST);

    wglSwapIntervalEXT(1);
}

void BasicGame::loop()
{
    float newTotalSeconds = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

    if (Running)
    {
        ElapsedSeconds = newTotalSeconds - previousTotalSeconds;
        TotalSeconds += ElapsedSeconds;
    }
    else
    {
        ElapsedSeconds = 0;
    }

    previousTotalSeconds = newTotalSeconds;

    update();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    draw();

    // swap buffers
    glutSwapBuffers();
}

void BasicGame::init()
{
}

void BasicGame::update()
{
}

void BasicGame::draw()
{
}

void BasicGame::loop_static()
{
    Instance->loop();
}
