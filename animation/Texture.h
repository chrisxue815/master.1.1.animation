#pragma once

#include <string>
#include <map>

#include <GL/glew.h>
#include <Magick++.h>

namespace chrisxue
{
    class Texture
    {
    public:
        static Texture *load(GLenum textureTarget, const std::string &texturePath);
        
        void bind(GLenum textureUnit);

    private:
        Texture(GLenum textureTarget, const std::string &texturePath);

        static std::map<std::string, Texture*> TextureMap;
        std::string texturePath;
        GLenum textureTarget;
        GLuint textureObj;
        Magick::Image *image;
        Magick::Blob blob;
    };
}
