#include "Camera.h"

#include <GL/freeglut.h>
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "Game.h"
#include "Character.h"
#include "Extensions.h"
#include "PhongEffect.h"
#include "OrenNayarEffect.h"

using namespace chrisxue;
using namespace glm;

Camera *Camera::Instance = NULL;
const float Camera::FOV = 60.0f;
const float Camera::ZNear = 1.0f;
const float Camera::ZFar = 1000.0f;

Camera::Camera()
{
    Instance = this;

    glutReshapeFunc(windowSizeChanged_static);
    glutMouseFunc(mouseChanged_static);
    glutMotionFunc(mouseMoved_static);
    glutMouseWheelFunc(mouseWheelSpun_static);
    glutKeyboardFunc(keyDown_static);
    glutKeyboardUpFunc(keyUp_static);

    initialUp = vec3(0, 1, 0);

    ZeroMem(WasKeyPressed);

    textureEnabled = false;
}

Camera::~Camera()
{
    if (Instance == this) Instance = NULL;
}

void Camera::init()
{
}

void Camera::update()
{
    TargetPosition = Target->position;
    position = TargetPosition + Offset;

    const float PhongSpecularPowerSpeed = 2;
    static float phongSpecularPower = 100;

    const float PhongSpecularIntensitySpeed = 1;
    static float phongSpecularIntensity = 1;

    const float OrenNayarAlbedoSpeed = 1;
    static float orenNayarAlbedo = 1;

    const float OrenNayarRoughnessSpeed = 0.1;
    static float orenNayarRoughness = 0;

    PhongEffect *phongEffect = game->teapots[Phong]->phongEffect;
    phongEffect->use();

    if (WasKeyPressed['w'])
    {
        phongSpecularPower += phongSpecularPower * PhongSpecularPowerSpeed * game->ElapsedSeconds;
        phongEffect->setSpecularPower(phongSpecularPower);
    }
    if (WasKeyPressed['s'])
    {
        phongSpecularPower -= phongSpecularPower * PhongSpecularPowerSpeed * game->ElapsedSeconds;
        phongEffect->setSpecularPower(phongSpecularPower);
    }
    if (WasKeyPressed['e'])
    {
        phongSpecularIntensity += PhongSpecularIntensitySpeed * game->ElapsedSeconds;
        phongEffect->setSpecularIntensity(phongSpecularIntensity);
    }
    if (WasKeyPressed['d'])
    {
        phongSpecularIntensity -= PhongSpecularIntensitySpeed * game->ElapsedSeconds;
        phongEffect->setSpecularIntensity(phongSpecularIntensity);
    }

    phongEffect->unuse();

    OrenNayarEffect *orenNayarEffect = game->teapots[OrenNayar]->orenNayarEffect;
    orenNayarEffect->use();

    if (WasKeyPressed['r'])
    {
        orenNayarAlbedo += OrenNayarAlbedoSpeed * game->ElapsedSeconds;
        orenNayarEffect->setAlbedo(orenNayarAlbedo);
    }
    if (WasKeyPressed['f'])
    {
        orenNayarAlbedo -= OrenNayarAlbedoSpeed * game->ElapsedSeconds;
        orenNayarEffect->setAlbedo(orenNayarAlbedo);
    }
    if (WasKeyPressed['t'])
    {
        orenNayarRoughness += OrenNayarRoughnessSpeed * game->ElapsedSeconds;
        orenNayarEffect->setRoughness(orenNayarRoughness);
    }
    if (WasKeyPressed['g'])
    {
        orenNayarRoughness -= OrenNayarRoughnessSpeed * game->ElapsedSeconds;
        orenNayarEffect->setRoughness(orenNayarRoughness);
    }

    orenNayarEffect->unuse();
}

void Camera::draw()
{
    View = lookAt(position, TargetPosition, initialUp);
}

void Camera::watch(Entity *target)
{
    Target = target;

    Offset = vec3(0, 20.0f, 50.0f);
}

mat4 Camera::VP()
{
    return Projection * View;
}

void Camera::windowSizeChanged(int w, int h)
{
    if(h == 0) h = 1;

    Width = w;
    Height = h;

    glViewport(0, 0, w, h);

    float ratio = (float)w / h;

    Projection = perspective(FOV, ratio, ZNear, ZFar);
}

void Camera::mouseChanged(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        Button = button;
        PreviousX = x;
        PreviousY = y;
        mouseMoved(x, y);
    }
}

void Camera::mouseMoved(int x, int y)
{
    const float RotationSpeed = -0.5f;  // degrees per pixel
    const float MovementSpeed = 0.001f;

    float xAngle = (x - PreviousX) * RotationSpeed;
    quat xRotation = angleAxis(xAngle, Target->initialUp);
    Offset = xRotation * Offset;

    vec3 right = cross(initialUp, Offset);
    right = safeNormalize(right);

    float yAngle = (y - PreviousY) * RotationSpeed;
    quat yRotation = angleAxis(yAngle, right);
    Offset = yRotation * Offset;

    if (Button == GLUT_RIGHT_BUTTON)
    {
        // also rotate Target
        vec3 newLooking(-Offset.x, 0, -Offset.z);
        Target->rotation = rotate(Target->initialLooking, newLooking);
        Target->originalLooking = newLooking;
    }

    PreviousX = x;
    PreviousY = y;
}

void Camera::mouseWheelSpun(int wheel, int direction, int x, int y)
{
    const float ZoomSpeed = -10.0f;

    float distance = length(Offset);
    float newDistance = distance + direction * ZoomSpeed;

    if (newDistance > 0.1f && newDistance < 100.0f)
    {
        // zoom in or out
        Offset = newDistance * safeNormalize(Offset);
    }
}

void Camera::keyDown(unsigned char key, int x, int y)
{
    if (WasKeyPressed[key]) return;

    WasKeyPressed[key] = true;

    switch (key)
    {
    case ' ':
        game->Running = !game->Running;
        break;
    case '1':
        textureEnabled = !textureEnabled;
        for (int i = 0; i < NumTeapotEffectType; i++)
        {
            game->teapots[i]->setTextureEnabled(textureEnabled);
        }
        break;
    case '2':
        for (int i = 0; i < NumTeapotEffectType; i++)
        {
            game->teapots[i]->setModelType(Classical);
        }
        break;
    case '3':
        for (int i = 0; i < NumTeapotEffectType; i++)
        {
            game->teapots[i]->setModelType(Orange);
        }
        break;
    case '4':
        for (int i = 0; i < NumTeapotEffectType; i++)
        {
            game->teapots[i]->setModelType(Mug);
        }
        break;
    case '5':
        for (int i = 0; i < NumTeapotEffectType; i++)
        {
            game->teapots[i]->setModelType(Earth);
        }
        break;
    }
}

void Camera::keyUp(unsigned char key, int x, int y)
{
    WasKeyPressed[key] = false;
    if (!Target) return;

    switch (key)
    {
    case 'w':
    case 's':
    case 'a':
    case 'd':
        //Target->stop();
        break;
    default:
        return;
    }
}

void Camera::windowSizeChanged_static(int w, int h)
{
    Instance->windowSizeChanged(w, h);
}

void Camera::mouseChanged_static(int Button, int state, int x, int y)
{
    Instance->mouseChanged(Button, state, x, y);
}

void Camera::mouseMoved_static(int x, int y)
{
    Instance->mouseMoved(x, y);
}

void Camera::mouseWheelSpun_static(int wheel, int direction, int x, int y)
{
    Instance->mouseWheelSpun(wheel, direction, x, y);
}

void Camera::keyDown_static(unsigned char key, int x, int y)
{
    Instance->keyDown(key, x, y);
}

void Camera::keyUp_static(unsigned char key, int x, int y)
{
    Instance->keyUp(key, x, y);
}
