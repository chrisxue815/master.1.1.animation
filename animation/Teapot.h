#pragma once

#include "Entity.h"

namespace chrisxue
{
    class ToonEffect;
    class PhongEffect;
    class OrenNayarEffect;

    enum TeapotEffectType {Tone, Phong, OrenNayar, NumTeapotEffectType};
    enum TeapotModelType {Classical, Orange, Mug, Earth, NumTeapotModelType};

	class Teapot : public Entity
	{
	public:
		Teapot(TeapotEffectType effectType);
		~Teapot(void);

		// override
        void init();
        void update();
        void draw();

        void setTextureEnabled(bool textureEnabled);
        void setModelType(TeapotModelType modelType);

		Model *model[NumTeapotModelType];

		ToonEffect *toonEffect;
        PhongEffect *phongEffect;
        OrenNayarEffect *orenNayarEffect;

    private:
        TeapotEffectType effectType;
        TeapotModelType modelType;
        GLenum textureUnit;
	};
}
