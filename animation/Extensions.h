#pragma once

#include <list>
#include <vector>
#include <map>

#include <assimp/scene.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#define arraySize(a) (sizeof(a) / sizeof(a[0]))
#define ZeroMem(a) (memset(a, 0, sizeof(a)))

#ifdef WIN32
#define snprintf _snprintf_s
#else
#endif

namespace chrisxue
{
    const float Pi = (float)3.14159265358979323846;
    const float PiOver2 = Pi / 2;
    const float PiOver3 = Pi / 3;
    const float PiOver4 = Pi / 4;
    const float PiOver6 = Pi / 6;
    const float TwoPi = Pi * 2;

    inline float toRadian(float x)
    {
        return (float)(x * Pi / 180.0f);
    }

    inline float toDegree(float x)
    {
        return (float)(x * 180.0f / Pi);
    }

    inline glm::vec3 toGlmVec3(const aiVector3D &v)
    {
        return glm::vec3(v.x, v.y, v.z);
    }

    inline glm::vec2 toGlmVec2(const aiVector3D &v)
    {
        return glm::vec2(v.x, v.y);
    }

    inline aiVector3D toAiVec3(const glm::vec3 &v)
    {
        return aiVector3D(v.x, v.y, v.z);
    }

    glm::mat4 toGlmMat4(const aiMatrix4x4 &m);

    glm::mat4 toGlmMat4(const aiMatrix3x3 &m);

    inline glm::quat toGlmQuat(const aiQuaternion &q)
    {
        return glm::quat(q.w, q.x, q.y, q.z);
    }

    inline glm::vec3 operator*(const glm::mat4 &m, const glm::vec3 &v)
    {
        return (glm::vec3)(m * glm::vec4(v, 1.0f));
    }

    inline glm::vec3 safeNormalize(const glm::vec3 &v)
    {
        return glm::length(v) > 0 ? glm::normalize(v) : glm::vec3(0, 1, 0);
    }

    inline glm::quat safeNormalize(const glm::quat &v)
    {
        return glm::length(v) > 0 ? glm::normalize(v) : v;
    }

    inline float acosD(const float cos)
    {
        return toDegree(acos(cos));
    }

    inline bool valid(glm::quat &q)
    {
        return (q.x == q.x && q.y == q.y && q.z == q.z);
    }

    glm::quat rotate(const glm::vec3 &source, const glm::vec3 &Target);

    void print(const glm::mat4 &m);

    template<class T>
    inline bool contains(const std::list<T> *collection, const T &element)
    {
        return find(collection->begin(), collection->end(), element) != collection->end();
    }

    template<class T>
    inline bool contains(const std::list<T> &collection, const T &element)
    {
        return find(collection.begin(), collection.end(), element) != collection.end();
    }

    template<class T>
    inline bool contains(const std::vector<T> *collection, const T &element)
    {
        return find(collection->begin(), collection->end(), element) != collection->end();
    }

    template<class T>
    inline bool contains(const std::vector<T> &collection, const T &element)
    {
        return find(collection.begin(), collection.end(), element) != collection.end();
    }

    template<class TKey, class TValue>
    inline bool contains(const std::map<TKey, TValue> *collection, const TKey &element)
    {
        return collection->find(element) != collection->end();
    }

    template<class TKey, class TValue>
    inline bool contains(const std::map<TKey, TValue> &collection, const TKey &element)
    {
        return collection.find(element) != collection.end();
    }
}
