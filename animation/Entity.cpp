#include "Entity.h"

#include <iostream>

#define _USE_MATH_DEFINES
#include <cmath>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <gl/glfx.h>

#include "Game.h"
#include "CharacterEffect.h"
#include "Model.h"

using namespace chrisxue;
using namespace glm;
using namespace std;

Entity::Entity()
{
    game = Game::Instance;
    moving = false;
    speed = 0;
    initialUp = vec3(0, 1, 0);
    initialLooking = vec3(0, 0, -1);
    scaling = vec3(1);
}

Entity::~Entity()
{
}

void Entity::init()
{

}

void Entity::update()
{
    if (moving)
    {
        vec3 velocity = speed * looking();
        position += velocity * game->ElapsedSeconds;
    }
}

void Entity::draw()
{
}

void Entity::move()
{
    moving = true;
}

void Entity::stop()
{
    moving = false;
}

vec3 Entity::right()
{
    return safeNormalize(rotation * glm::cross(initialLooking, initialUp));
}

vec3 Entity::originalRight()
{
    return safeNormalize(glm::cross(originalLooking, initialUp));
}

void Entity::looking(const glm::vec3 &newLooking)
{
    rotation = rotate(initialLooking, newLooking);
}
