#pragma once

#include "entity.h"

#include <map>
#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <assimp/scene.h>

#include "SkeletonAnimation.h"

typedef unsigned int GLuint;

namespace chrisxue
{
    enum CharacterName { XinZhao, JarvanIV };
    enum CharacterState { Idle1, Idle2, Run, Attack1, Dance, NumStates };

    class CharacterEffect;
    class JointTransform;
    struct TransformMap;

    class Character : public Entity
    {
    public:
        Character();
        ~Character();

        // override
        void init(CharacterName name);
        void update();
        void draw();

        // override
        void move();
        void stop();

        void track(Character *Target) { this->Target = Target; }

        CharacterEffect *effect;
        CharacterState state;

    private:
        void updateTracking();
        void stateChanged();

        Model *model[NumStates];
        CharacterState previousState;
        float animationStartTime;

        // interpolation
        static const float InterpolationTime;
        float interpolationEndTime;
        TransformMap transform;
        TransformMap nextTransform;

        Character *Target;
    };
}
