#pragma once

#include <vector>
#include <map>
#include <string>

#include <gl/glew.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>

#include "Extensions.h"

namespace chrisxue
{
    enum VertexBufferType
    {
        IndexBuffer,
        PositionBuffer,
        NormalBuffer,
        TexcoordBuffer,
        JointBuffer,
        NumVertexBufferTypes            
    };

    struct MeshEntry
    {
        static const unsigned int InvalidMaterial = -1;
        unsigned int NumIndices;
        unsigned int BaseVertex;
        unsigned int BaseIndex;
        unsigned int MaterialIndex;

        MeshEntry()
        {
            NumIndices = 0;
            BaseVertex = 0;
            BaseIndex = 0;
            MaterialIndex = InvalidMaterial;
        }
    };

    struct VertexJointData
    {
        static const int NumJointsPerVertex = 4;
        unsigned int JointIds[NumJointsPerVertex];
        float Weights[NumJointsPerVertex];

        VertexJointData()
        {
            reset();
        };

        void reset()
        {
            ZeroMem(JointIds);
            ZeroMem(Weights);
        }

        void addJointData(unsigned int JointId, float Weight);
    };

    class Texture;
    class SkeletonAnimation;

    class Model
    {
    public:
        ~Model();

        static Model *load(const char *path);
        void draw();
        void draw(GLenum textureUnit);

        SkeletonAnimation *Animation;

        const aiScene *scene;
        std::vector<MeshEntry> meshes;

    private:
        Model(Assimp::Importer *importer, const aiScene *scene, const char *modelPath);

        void initBuffers();

        bool initFromScene(const aiScene *scene, const std::string &modelPath);
        void initMesh(unsigned int meshIndex, const aiMesh *mesh);
        void initMaterials();

        GLuint vao;
        GLuint buffers[NumVertexBufferTypes];
        std::vector<Texture*> textures;

        Assimp::Importer *importer;
        std::string modelPath;

        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;
        std::vector<VertexJointData> joints;
        std::vector<glm::uint> indices;
    };
}
