#pragma once

#include <map>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <assimp/scene.h>

namespace chrisxue
{
    class Game;
    class Model;

    class Entity
    {
    public:
        Entity();
        ~Entity();

        virtual void init();
        virtual void update();
        virtual void draw();

        virtual void move();
        virtual void stop();

        virtual glm::vec3 looking() { return rotation * initialLooking; }
        virtual void looking(const glm::vec3 &newLooking);
        virtual glm::vec3 up() { return rotation * initialUp; }
        virtual glm::vec3 right();
        virtual glm::vec3 originalRight();

        Game *game;

        glm::vec3 scaling;
        glm::quat rotation;
        glm::vec3 position;

        glm::vec3 initialLooking;
        glm::vec3 initialUp;

        glm::vec3 originalLooking;

    protected:
        bool moving;
        float speed;
    };
}
