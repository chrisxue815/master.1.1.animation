#pragma once

#include <vector>
#include <map>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <assimp/scene.h>

namespace chrisxue
{
    class Model;
    class JointTransform;
    struct VertexJointData;

    struct Transform
    {
        glm::vec3 Scaling;
        glm::quat Rotation;
        glm::vec3 Translation;

        Transform()
        {
            Scaling = glm::vec3(1, 1, 1);
            Rotation = glm::quat(1, 0, 0, 0);
        }
    };

    struct TransformMap
    {
        std::map<std::string, Transform> Map;
    };

    class SkeletonAnimation
    {
    public:
        SkeletonAnimation(Model *model);
        ~SkeletonAnimation(void);

        void loadJoints(unsigned int meshIndex, const aiMesh *mesh, std::vector<VertexJointData> &joints);

        const std::vector<JointTransform> &transformJoints(float totalSeconds);

        void interpolate(TransformMap &t1,
            TransformMap &t2,
            float amount,
            std::vector<JointTransform> &out);

        unsigned int NumJoints;
        TransformMap AnimatedNodes;
        std::vector<JointTransform> JointTransforms;

    private:
        void updateNodeTree(float time, const aiNode *node, const glm::mat4 &parentTransform);
        void updateNodeTree(const aiNode *node, const glm::mat4 &parentTransform);

        const aiNodeAnim *findNodeAnim(const aiAnimation *animation, const std::string nodeName);

        Model *model;
        const aiScene *scene;

        std::map<std::string, unsigned int> jointMap;

        glm::mat4 globalInverseTransform;

        TransformMap *t1;
        TransformMap *t2;
        float amount;
        std::vector<JointTransform> *out;
    };
}
