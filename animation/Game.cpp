#include "Game.h"

#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>

#include "Camera.h"
#include "GridLineGround.h"
#include "Teapot.h"

using namespace chrisxue;
using namespace glm;
using namespace std;

Game *Game::Instance = NULL;

Game::Game()
{
    Instance = this;
    Width = 1366;
    Height = 768;
}

Game::~Game()
{
    delete ground;
    delete camera;
    for (int i = 0; i < NumTeapotEffectType; i++)
    {
        delete teapots[i];
    }

    if (Instance == this) Instance = NULL;
}

void Game::init()
{
    //glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
    glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

    // Set up your objects and shaders
    initObjects();
}

void Game::initObjects()
{
    camera = new Camera();
    camera->init();

    ground = new GridLineGround();
    ground->init();

    for (int i = 0; i < NumTeapotEffectType; i++)
    {
        teapots[i] = new Teapot((TeapotEffectType)i);
        teapots[i]->init();
        teapots[i]->position = vec3(i*20, 0, 0);
    }

    camera->watch(teapots[Phong]);
}

void Game::update()
{
    camera->update();

    ground->update();

    for (int i = 0; i < NumTeapotEffectType; i++)
    {
        teapots[i]->update();
    }
}

void Game::draw()
{
    camera->draw();

    ground->draw();

    for (int i = 0; i < NumTeapotEffectType; i++)
    {
        teapots[i]->draw();
    }
}
