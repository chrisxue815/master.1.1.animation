#pragma once

#include "Effect.h"

#include <glm/glm.hpp>

#include "Light.h"

namespace chrisxue
{
	class OrenNayarEffect : public Effect
	{
	public:
		OrenNayarEffect(void);
		~OrenNayarEffect(void);

		//override
		void init();
		void update();
		void draw();

        void setWVP(const glm::mat4 &wvp);
        void setWorldTransform(const glm::mat4 &worldTransform);
        void setDirectionalLight(const DirectionalLight &light);
        void setCameraPosition(const glm::vec3 &cameraPosition);
        void setSpecularIntensity(float intensity);
        void setSpecularPower(float power);
        void setAlbedo(float albedo);
        void setRoughness(float roughness);
        void setTextureEnabled(bool textureEnabled);
        void setTexture(unsigned int texture);

    private:
        GLuint WVPLocation;
        GLuint WorldTransformLocation;
        GLuint CameraPositionLocation;
        GLuint SpecularIntensityLocation;
        GLuint SpecularPowerLocation;
        GLuint AlbedoLocation;
        GLuint RoughnessLocation;
        GLuint TextureEnabledLocation;
        GLuint TextureLocation;

        struct
        {
            GLuint Color;
            GLuint AmbientIntensity;
            GLuint DiffuseIntensity;
            GLuint Direction;
        } DirLightLocation;
	};
}
