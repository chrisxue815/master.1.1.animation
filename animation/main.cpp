#include "Game.h"
#include <iostream>

using namespace chrisxue;

int main(int argc, char** argv)
{
    Game game;
    game.exec(&argc, argv);

    return 0;
}
