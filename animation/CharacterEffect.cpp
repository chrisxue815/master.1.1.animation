#include "CharacterEffect.h"

#include <limits.h>
#include <string>
#include <assert.h>

#include <gl/glfx.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace chrisxue;
using namespace glm;
using namespace std;

static const char *EffectFile = "contents/shaders/character.glsl";

CharacterEffect::CharacterEffect() : Effect(EffectFile)
{
}

void CharacterEffect::init()
{
    compileProgram("Character");

    WVPLocation = getUniformLocation("WVP");
    WorldMatrixLocation = getUniformLocation("World");
    ColorTextureLocation = getUniformLocation("ColorMap");
    CameraPositionLocation = getUniformLocation("CameraPosition");
    DirLightLocation.Color = getUniformLocation("DirectionalLight.Base.Color");
    DirLightLocation.AmbientIntensity = getUniformLocation("DirectionalLight.Base.AmbientIntensity");
    DirLightLocation.Direction = getUniformLocation("DirectionalLight.Direction");
    DirLightLocation.DiffuseIntensity = getUniformLocation("DirectionalLight.Base.DiffuseIntensity");
    MatSpecularIntensityLocation = getUniformLocation("MatSpecularIntensity");
    MatSpecularPowerLocation = getUniformLocation("SpecularPower");
    NumPointLightsLocation = getUniformLocation("NumPointLights");
    NumSpotLightsLocation = getUniformLocation("NumSpotLights");

    if (DirLightLocation.AmbientIntensity == InvalidUniformLocation ||
        WVPLocation == InvalidUniformLocation ||
        WorldMatrixLocation == InvalidUniformLocation ||
        ColorTextureLocation == InvalidUniformLocation ||
        CameraPositionLocation == InvalidUniformLocation ||
        DirLightLocation.Color == InvalidUniformLocation ||
        DirLightLocation.DiffuseIntensity == InvalidUniformLocation ||
        DirLightLocation.Direction == InvalidUniformLocation ||
        MatSpecularIntensityLocation == InvalidUniformLocation ||
        MatSpecularPowerLocation == InvalidUniformLocation ||
        NumPointLightsLocation == InvalidUniformLocation ||
        NumSpotLightsLocation == InvalidUniformLocation)
    {
        fprintf(stderr, "Error initializing the character effect\n");
        throw 1;
    }

    for (unsigned int i = 0; i < arraySize(PointLightsLocation); i++)
    {
        char name[128];
        memset(name, 0, sizeof(name));
        snprintf(name, sizeof(name), "PointLights[%d].Base.Color", i);
        PointLightsLocation[i].Color = getUniformLocation(name);

        snprintf(name, sizeof(name), "PointLights[%d].Base.AmbientIntensity", i);
        PointLightsLocation[i].AmbientIntensity = getUniformLocation(name);

        snprintf(name, sizeof(name), "PointLights[%d].Position", i);
        PointLightsLocation[i].Position = getUniformLocation(name);

        snprintf(name, sizeof(name), "PointLights[%d].Base.DiffuseIntensity", i);
        PointLightsLocation[i].DiffuseIntensity = getUniformLocation(name);

        snprintf(name, sizeof(name), "PointLights[%d].Atten.Constant", i);
        PointLightsLocation[i].Atten.Constant = getUniformLocation(name);

        snprintf(name, sizeof(name), "PointLights[%d].Atten.Linear", i);
        PointLightsLocation[i].Atten.Linear = getUniformLocation(name);

        snprintf(name, sizeof(name), "PointLights[%d].Atten.Exp", i);
        PointLightsLocation[i].Atten.Exp = getUniformLocation(name);

        if (PointLightsLocation[i].Color == InvalidUniformLocation ||
            PointLightsLocation[i].AmbientIntensity == InvalidUniformLocation ||
            PointLightsLocation[i].Position == InvalidUniformLocation ||
            PointLightsLocation[i].DiffuseIntensity == InvalidUniformLocation ||
            PointLightsLocation[i].Atten.Constant == InvalidUniformLocation ||
            PointLightsLocation[i].Atten.Linear == InvalidUniformLocation ||
            PointLightsLocation[i].Atten.Exp == InvalidUniformLocation)
        {
            fprintf(stderr, "Error initializing the character effect\n");
            throw 1;
        }
    }

    for (unsigned int i = 0; i < arraySize(SpotLightsLocation); i++)
    {
        char name[128];
        memset(name, 0, sizeof(name));
        snprintf(name, sizeof(name), "SpotLights[%d].Base.Base.Color", i);
        SpotLightsLocation[i].Color = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Base.Base.AmbientIntensity", i);
        SpotLightsLocation[i].AmbientIntensity = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Base.Position", i);
        SpotLightsLocation[i].Position = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Direction", i);
        SpotLightsLocation[i].Direction = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Cutoff", i);
        SpotLightsLocation[i].Cutoff = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Base.Base.DiffuseIntensity", i);
        SpotLightsLocation[i].DiffuseIntensity = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Base.Atten.Constant", i);
        SpotLightsLocation[i].Atten.Constant = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Base.Atten.Linear", i);
        SpotLightsLocation[i].Atten.Linear = getUniformLocation(name);

        snprintf(name, sizeof(name), "SpotLights[%d].Base.Atten.Exp", i);
        SpotLightsLocation[i].Atten.Exp = getUniformLocation(name);

        if (SpotLightsLocation[i].Color == InvalidUniformLocation ||
            SpotLightsLocation[i].AmbientIntensity == InvalidUniformLocation ||
            SpotLightsLocation[i].Position == InvalidUniformLocation ||
            SpotLightsLocation[i].Direction == InvalidUniformLocation ||
            SpotLightsLocation[i].Cutoff == InvalidUniformLocation ||
            SpotLightsLocation[i].DiffuseIntensity == InvalidUniformLocation ||
            SpotLightsLocation[i].Atten.Constant == InvalidUniformLocation ||
            SpotLightsLocation[i].Atten.Linear == InvalidUniformLocation ||
            SpotLightsLocation[i].Atten.Exp == InvalidUniformLocation)
        {
            fprintf(stderr, "Error initializing the lighting technique\n");
            throw 1;
        }
    }

    for (unsigned int i = 0; i < arraySize(JointLocation); i++)
    {
        char name[128];
        memset(name, 0, sizeof(name));
        snprintf(name, sizeof(name), "Joints[%d]", i);
        JointLocation[i] = getUniformLocation(name);
    }

    use();

    setColorTextureUnit(0);

    DirectionalLight directionalLight;
    directionalLight.Color = vec3(1.0f, 1.0f, 1.0f);
    directionalLight.AmbientIntensity = 0.55f;
    directionalLight.DiffuseIntensity = 0.9f;
    directionalLight.Direction = vec3(-1, -1, -1);
    setDirectionalLight(directionalLight);

    setMatSpecularIntensity(0.0f);
    setMatSpecularPower(0);
}

void CharacterEffect::setWVP(const mat4 &wvp)
{
    glUniformMatrix4fv(WVPLocation, 1, false, value_ptr(wvp));
}

void CharacterEffect::setWorldMatrix(const mat4 &world)
{
    glUniformMatrix4fv(WorldMatrixLocation, 1, false, value_ptr(world));
}

void CharacterEffect::setColorTextureUnit(unsigned int textureUnit)
{
    glUniform1i(ColorTextureLocation, textureUnit);
}

void CharacterEffect::setDirectionalLight(const DirectionalLight &light)
{
    glUniform3f(DirLightLocation.Color, light.Color.x, light.Color.y, light.Color.z);
    glUniform1f(DirLightLocation.AmbientIntensity, light.AmbientIntensity);
    vec3 Direction = light.Direction;
    Direction = safeNormalize(Direction);
    glUniform3f(DirLightLocation.Direction, Direction.x, Direction.y, Direction.z);
    glUniform1f(DirLightLocation.DiffuseIntensity, light.DiffuseIntensity);
}

void CharacterEffect::setCameraPosition(const vec3 &cameraPosition)
{
    glUniform3f(CameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
}

void CharacterEffect::setMatSpecularIntensity(float intensity)
{
    glUniform1f(MatSpecularIntensityLocation, intensity);
}

void CharacterEffect::setMatSpecularPower(float power)
{
    glUniform1f(MatSpecularPowerLocation, power);
}

void CharacterEffect::setPointLights(unsigned int numLights, const PointLight *lights)
{
    glUniform1i(NumPointLightsLocation, numLights);

    for (unsigned int i = 0; i < numLights; i++)
    {
        glUniform3f(PointLightsLocation[i].Color, lights[i].Color.x, lights[i].Color.y, lights[i].Color.z);
        glUniform1f(PointLightsLocation[i].AmbientIntensity, lights[i].AmbientIntensity);
        glUniform1f(PointLightsLocation[i].DiffuseIntensity, lights[i].DiffuseIntensity);
        glUniform3f(PointLightsLocation[i].Position, lights[i].Position.x, lights[i].Position.y, lights[i].Position.z);
        glUniform1f(PointLightsLocation[i].Atten.Constant, lights[i].Attenuation.Constant);
        glUniform1f(PointLightsLocation[i].Atten.Linear, lights[i].Attenuation.Linear);
        glUniform1f(PointLightsLocation[i].Atten.Exp, lights[i].Attenuation.Exp);
    }
}

void CharacterEffect::setSpotLights(unsigned int numLights, const SpotLight *lights)
{
    glUniform1i(NumSpotLightsLocation, numLights);

    for (unsigned int i = 0; i < numLights; i++)
    {
        glUniform3f(SpotLightsLocation[i].Color, lights[i].Color.x, lights[i].Color.y, lights[i].Color.z);
        glUniform1f(SpotLightsLocation[i].AmbientIntensity, lights[i].AmbientIntensity);
        glUniform1f(SpotLightsLocation[i].DiffuseIntensity, lights[i].DiffuseIntensity);
        glUniform3f(SpotLightsLocation[i].Position,  lights[i].Position.x, lights[i].Position.y, lights[i].Position.z);
        vec3 Direction = lights[i].Direction;
        Direction = safeNormalize(Direction);
        glUniform3f(SpotLightsLocation[i].Direction, Direction.x, Direction.y, Direction.z);
        glUniform1f(SpotLightsLocation[i].Cutoff, cosf(toRadian(lights[i].Cutoff)));
        glUniform1f(SpotLightsLocation[i].Atten.Constant, lights[i].Attenuation.Constant);
        glUniform1f(SpotLightsLocation[i].Atten.Linear,   lights[i].Attenuation.Linear);
        glUniform1f(SpotLightsLocation[i].Atten.Exp,      lights[i].Attenuation.Exp);
    }
}

void CharacterEffect::setJointTransform(uint index, const mat4 &transform)
{
    assert(index < MaxNumJoints);
    glUniformMatrix4fv(JointLocation[index], 1, false, value_ptr(transform));
}
