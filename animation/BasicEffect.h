#pragma once

#include "Effect.h"

#include <glm/glm.hpp>

namespace chrisxue
{
    class BasicEffect : public Effect
    {
    public:
        BasicEffect(void);
        ~BasicEffect(void);

        void setWVP(const glm::mat4 &wvp);

    private:
        GLuint WVPLocation;
    };
}
