#include "SkeletonAnimation.h"

#include <string>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include "JointTransform.h"
#include "Extensions.h"
#include "Model.h"

using namespace chrisxue;
using namespace glm;
using namespace std;

SkeletonAnimation::SkeletonAnimation(Model *model)
{
    this->model = model;
    scene = model->scene;

    NumJoints = 0;

    globalInverseTransform = toGlmMat4(scene->mRootNode->mTransformation);
    globalInverseTransform = inverse(globalInverseTransform);
}

SkeletonAnimation::~SkeletonAnimation(void)
{
}

void SkeletonAnimation::loadJoints(uint meshIndex, const aiMesh *mesh, vector<VertexJointData> &joints)
{
    for (uint i = 0; i < mesh->mNumBones; i++)
    {
        uint jointId = 0;
        string jointName(mesh->mBones[i]->mName.data);

        if (!contains(jointMap, jointName))
        {
            jointId = NumJoints;
            NumJoints++;
            JointTransforms.push_back(JointTransform());
            JointTransforms[jointId].ToJointSpace = toGlmMat4(mesh->mBones[i]->mOffsetMatrix);
            jointMap[jointName] = jointId;
        }
        else
        {
            jointId = jointMap[jointName];
        }

        for (uint j = 0; j < mesh->mBones[i]->mNumWeights; j++)
        {
            aiVertexWeight &vertexWeight = mesh->mBones[i]->mWeights[j];
            uint vertexId = model->meshes[meshIndex].BaseVertex + vertexWeight.mVertexId;
            float weight  = vertexWeight.mWeight;
            joints[vertexId].addJointData(jointId, weight);
        }
    }
}

const vector<JointTransform> &SkeletonAnimation::transformJoints(float totalSeconds)
{
    mat4 identity;

    float ticksPerSecond = (float)scene->mAnimations[0]->mTicksPerSecond;
    if (ticksPerSecond == 0) ticksPerSecond = 25.0f;

    float totalTicks = totalSeconds * ticksPerSecond;
    float ticks = fmod(totalTicks, (float)scene->mAnimations[0]->mDuration);

    AnimatedNodes.Map.clear();

    updateNodeTree(ticks, scene->mRootNode, identity);

    return JointTransforms;
}

void SkeletonAnimation::updateNodeTree(float ticks, const aiNode *node, const mat4 &parentTransform)
{
    string nodeName(node->mName.data);

    const aiAnimation *animation = scene->mAnimations[0];
    const aiNodeAnim *nodeAnim = findNodeAnim(animation, nodeName);

    vec3 scaling;
    quat rotation;
    vec3 translation;
    mat4 nodeTransform = toGlmMat4(node->mTransformation);

    // if this node has animation
    if (nodeAnim)
    {
        // interpolate scaling and generate scaling matrix
        scaling = JointTransform::interpolateScaling(ticks, nodeAnim);
        mat4 scalingTransform = scale(scaling);

        // interpolate rotation and generate rotation matrix
        rotation = JointTransform::interpolateRotation(ticks, nodeAnim);
        mat4 rotationTransform = toMat4(rotation);

        // interpolate translation and generate translation matrix
        translation = JointTransform::interpolatePosition(ticks, nodeAnim);
        mat4 translationTransform = translate(translation);

        nodeTransform = translationTransform * rotationTransform * scalingTransform;
        Transform transform;
        transform.Scaling = scaling;
        transform.Rotation = rotation;
        transform.Translation = translation;
        AnimatedNodes.Map[nodeName] = transform;
    }

    // combine the above transformations
    mat4 globalTransform = parentTransform * nodeTransform;

    // if this node is a joint
    if (contains(jointMap, nodeName))
    {
        uint jointId = jointMap[nodeName];
        JointTransform &transform = JointTransforms[jointId];
        transform.Scaling = scaling;
        transform.Rotation = rotation;
        transform.Translation = translation;
        transform.FinalTransform = globalInverseTransform * globalTransform * transform.ToJointSpace;
    }

    for (uint i = 0; i < node->mNumChildren; i++)
    {
        updateNodeTree(ticks, node->mChildren[i], globalTransform);
    }
}

const aiNodeAnim *SkeletonAnimation::findNodeAnim(const aiAnimation *animation, const string nodeName)
{
    for (uint i = 0; i < animation->mNumChannels; i++)
    {
        const aiNodeAnim *nodeAnim = animation->mChannels[i];

        if (string(nodeAnim->mNodeName.data) == nodeName)
        {
            return nodeAnim;
        }
    }

    return NULL;
}

void SkeletonAnimation::interpolate(TransformMap &t1,
                                    TransformMap &t2,
                                    float amount,
                                    vector<JointTransform> &out)
{
    mat4 identity;

    this->t1 = &t1;
    this->t2 = &t2;
    this->amount = amount;
    this->out = &out;

    updateNodeTree(scene->mRootNode, identity);
}

void SkeletonAnimation::updateNodeTree(const aiNode *node, const mat4 &parentTransform)
{
    string nodeName(node->mName.data);

    const aiAnimation *animation = scene->mAnimations[0];
    const aiNodeAnim *nodeAnim = findNodeAnim(animation, nodeName);

    vec3 scaling;
    quat rotation;
    vec3 translation;
    mat4 nodeTransform = toGlmMat4(node->mTransformation);

    // if this node has animation
    if (nodeAnim)
    {
        Transform &transform1 = (*t1).Map[nodeName];
        Transform &transform2 = (*t2).Map[nodeName];

        // Interpolate scaling and generate scaling transformation matrix
        scaling = transform1.Scaling + amount * (transform2.Scaling - transform1.Scaling);
        //scaling = transform1.Scaling;
        mat4 scalingTransform = scale(scaling);

        // Interpolate rotation and generate rotation transformation matrix
        rotation = slerp(transform1.Rotation, transform2.Rotation, amount);
        //rotation = transform1.Rotation;
        mat4 rotationTransform = toMat4(rotation);

        // Interpolate translation and generate translation transformation matrix
        translation = transform1.Translation + amount * (transform2.Translation - transform1.Translation);
        //translation = transform1.Translation;
        mat4 translationTransform = translate(translation);

        nodeTransform = translationTransform * rotationTransform * scalingTransform;
    }

    // Combine the above transformations
    mat4 globalTransform = parentTransform * nodeTransform;

    // if it's a joint
    if (contains(jointMap, nodeName))
    {
        uint jointId = jointMap[nodeName];
        JointTransform &transform = (*out)[jointId];
        transform.Scaling = scaling;
        transform.Rotation = rotation;
        transform.Translation = translation;
        transform.FinalTransform = globalInverseTransform * globalTransform * transform.ToJointSpace;
    }

    for (uint i = 0; i < node->mNumChildren; i++)
    {
        updateNodeTree(node->mChildren[i], globalTransform);
    }
}
