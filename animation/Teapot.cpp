#include "Teapot.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "Game.h"
#include "Camera.h"
#include "ToonEffect.h"
#include "PhongEffect.h"
#include "OrenNayarEffect.h"
#include "Model.h"
#include "Extensions.h"

using namespace chrisxue;
using namespace glm;

Teapot::Teapot(TeapotEffectType effectType)
{
    this->effectType = effectType;
    setModelType(Classical);
}

Teapot::~Teapot(void)
{
}

void Teapot::init()
{
    model[Classical] = Model::load("contents/models/teapot.dae");
    model[Orange] = Model::load("contents/models/orange/Orange.dae");
    model[Mug] = Model::load("contents/models/mug/coffeMug1_free_collada.dae");
    model[Earth] = Model::load("contents/models/dices.dae");

    switch (effectType)
    {
    case Tone:
        toonEffect = new ToonEffect();
        toonEffect->init();
        break;
    case Phong:
        phongEffect = new PhongEffect();
        phongEffect->init();
        break;
    case OrenNayar:
        orenNayarEffect = new OrenNayarEffect();
        orenNayarEffect->init();
        break;
    }

    Entity::init();
}

void Teapot::update()
{
    const float RotationSpeed = -90.f;
    float angle = RotationSpeed * game->ElapsedSeconds;
    quat deltaRotation = angleAxis(angle, 0.f, 1.f, 0.f);
    rotation = deltaRotation * rotation;
}

void Teapot::draw()
{
    mat4 worldTransform = translate(position) * toMat4(rotation) * scale(scaling);
    mat4 wvp = game->camera->VP() * worldTransform;

    switch (effectType)
    {
    case Tone:
        toonEffect->use();
        toonEffect->setWorldTransform(worldTransform);
        toonEffect->setWVP(wvp);

        model[modelType]->draw(textureUnit);

        toonEffect->unuse();
        break;
    case Phong:
        phongEffect->use();
        phongEffect->setWorldTransform(worldTransform);
        phongEffect->setCameraPosition(game->camera->position);
        phongEffect->setWVP(wvp);

        model[modelType]->draw(textureUnit);

        phongEffect->unuse();
        break;
    case OrenNayar:
        orenNayarEffect->use();
        orenNayarEffect->setWorldTransform(worldTransform);
        orenNayarEffect->setCameraPosition(game->camera->position);
        orenNayarEffect->setWVP(wvp);

        model[modelType]->draw(textureUnit);

        orenNayarEffect->unuse();
        break;
    }
}

void Teapot::setTextureEnabled(bool textureEnabled)
{
    switch (effectType)
    {
    case Tone:
        toonEffect->use();
        toonEffect->setTextureEnabled(textureEnabled);
        toonEffect->unuse();
        break;
    case Phong:
        phongEffect->use();
        phongEffect->setTextureEnabled(textureEnabled);
        phongEffect->unuse();
        break;
    case OrenNayar:
        orenNayarEffect->use();
        orenNayarEffect->setTextureEnabled(textureEnabled);
        orenNayarEffect->unuse();
        break;
    }
}

void Teapot::setModelType(TeapotModelType modelType)
{
    this->modelType = modelType;

    switch (modelType)
    {
    case Classical:
        scaling = vec3(3.f);
        rotation = angleAxis(-90.f, 1.f, 0.f, 0.f);
        textureUnit = GL_TEXTURE0;
        break;
    case Orange:
        scaling = vec3(5.f);
        rotation = quat();
        textureUnit = GL_TEXTURE0;
        break;
    case Mug:
        scaling = vec3(0.3f);
        rotation = quat();
        textureUnit = GL_TEXTURE0;
        break;
    case Earth:
        scaling = vec3(0.01f);
        rotation = quat();
        textureUnit = GL_TEXTURE1;
        break;
    }
}
