#include "Effect.h"

#include <stdio.h>
#include <string>
#include <assert.h>
#include <gl/glfx.h>

using namespace chrisxue;
using namespace std;

Effect::Effect(const char *effectFile)
{
    this->effectFile = effectFile;
    shaderProgram = 0;
    effect = glfxGenEffect();
}

Effect::~Effect()
{
    if (shaderProgram != 0)
    {
        glDeleteProgram(shaderProgram);
        shaderProgram = 0;
    }

    glfxDeleteEffect(effect);
}

void Effect::compileProgram(const char *program)
{
    if (!glfxParseEffectFromFile(effect, effectFile))
    {
        string log = glfxGetEffectLog(effect);
        fprintf(stderr, "Error creating effect from file '%s':\n", effectFile);
        fprintf(stderr, "%s\n", log.c_str());
        throw 1;
    }

    shaderProgram = glfxCompileProgram(effect, program);

    if (shaderProgram < 0)
    {
        string log = glfxGetEffectLog(effect);
        fprintf(stderr, "Error compiling program '%s' in effect file '%s':\n", program, effectFile);
        fprintf(stderr, "%s\n", log.c_str());
        throw 1;
    }
}

void Effect::use()
{
    glUseProgram(shaderProgram);
}

void Effect::unuse()
{
    glUseProgram(0);
}

GLint Effect::getUniformLocation(const char* uniformName)
{
    GLuint location = glGetUniformLocation(shaderProgram, uniformName);

    if (location == InvalidUniformLocation)
    {
        fprintf(stderr, "Warning! Unable to get the location of uniform '%s'\n", uniformName);
    }

    return location;
}

GLint Effect::getProgramParam(GLint param)
{
    GLint ret;
    glGetProgramiv(shaderProgram, param, &ret);
    return ret;
}
