#pragma once

#include "Entity.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace chrisxue
{
    class Character;

    class Camera : public Entity
    {
    public:
        Camera();
        ~Camera();

        // override
        void init();
        void update();
        void draw();

        void watch(Entity *target);

        glm::mat4 VP();

        glm::mat4 View;
        glm::mat4 Projection;

        // camera
        glm::vec3 TargetPosition;

        // viewport
        int Width;
        int Height;
        static const float FOV;
        static const float ZNear;
        static const float ZFar;

    private:
        static void windowSizeChanged_static(int w, int h);
        static void mouseChanged_static(int Button, int state, int x, int y);
        static void mouseMoved_static(int x, int y);
        static void mouseWheelSpun_static(int wheel, int direction, int x, int y);
        static void keyDown_static(unsigned char key, int x, int y);
        static void keyUp_static(unsigned char key, int x, int y);

        void windowSizeChanged(int w, int h);
        void mouseChanged(int Button, int state, int x, int y);
        void mouseMoved(int x, int y);
        void mouseWheelSpun(int wheel, int direction, int x, int y);
        void keyDown(unsigned char key, int x, int y);
        void keyUp(unsigned char key, int x, int y);

        static Camera *Instance;

        // mouse
        int Button;
        int PreviousX;
        int PreviousY;

        bool WasKeyPressed[256];

        Entity *Target;
        glm::vec3 Offset;

        bool textureEnabled;
    };
}
