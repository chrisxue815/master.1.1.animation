#include "JointTransform.h"

#include "Extensions.h"

using namespace chrisxue;
using namespace glm;
using namespace std;

JointTransform::JointTransform()
{
}

vec3 JointTransform::interpolateScaling(float ticks, const aiNodeAnim *nodeAnim)
{
    if (nodeAnim->mNumScalingKeys == 1)
    {
        return toGlmVec3(nodeAnim->mScalingKeys[0].mValue);
    }

    uint scalingIndex = findScaling(ticks, nodeAnim);
    uint nextScalingIndex = scalingIndex + 1;

    aiVectorKey &scalingKey = nodeAnim->mScalingKeys[scalingIndex];
    aiVectorKey &nextScalingKey = nodeAnim->mScalingKeys[nextScalingIndex];

    float deltaTime = (float)(nextScalingKey.mTime - scalingKey.mTime);
    float factor = (ticks - (float)scalingKey.mTime) / deltaTime;

    vec3 start = toGlmVec3(scalingKey.mValue);
    vec3 end = toGlmVec3(nextScalingKey.mValue);
    vec3 delta = end - start;

    return start + factor * delta;
}

quat JointTransform::interpolateRotation(float ticks, const aiNodeAnim *nodeAnim)
{
    if (nodeAnim->mNumRotationKeys == 1)
    {
        return toGlmQuat(nodeAnim->mRotationKeys[0].mValue);
    }

    uint rotationIndex = findRotation(ticks, nodeAnim);
    uint nextRotationIndex = rotationIndex + 1;

    aiQuatKey &rotationKey = nodeAnim->mRotationKeys[rotationIndex];
    aiQuatKey &nextRotationKey = nodeAnim->mRotationKeys[nextRotationIndex];

    float deltaTime = (float)(nextRotationKey.mTime - rotationKey.mTime);
    float factor = (ticks - (float)rotationKey.mTime) / deltaTime;

    quat start = toGlmQuat(rotationKey.mValue);
    quat end = toGlmQuat(nextRotationKey.mValue);

    return safeNormalize(slerp(start, end, factor));
}

vec3 JointTransform::interpolatePosition(float ticks, const aiNodeAnim *nodeAnim)
{
    if (nodeAnim->mNumPositionKeys == 1)
    {
        return toGlmVec3(nodeAnim->mPositionKeys[0].mValue);
    }

    uint positionIndex = findPosition(ticks, nodeAnim);
    uint nextPositionIndex = positionIndex + 1;

    aiVectorKey &positionKey = nodeAnim->mPositionKeys[positionIndex];
    aiVectorKey &nextPositionKey = nodeAnim->mPositionKeys[nextPositionIndex];

    float deltaTime = (float)(nextPositionKey.mTime - positionKey.mTime);
    float factor = (ticks - (float)positionKey.mTime) / deltaTime;

    vec3 start = toGlmVec3(positionKey.mValue);
    vec3 end = toGlmVec3(nextPositionKey.mValue);
    vec3 delta = end - start;

    return start + factor * delta;
}

uint JointTransform::findScaling(float ticks, const aiNodeAnim *nodeAnim)
{
    for (uint i = 0; i < nodeAnim->mNumScalingKeys - 1; i++)
    {
        if (ticks < (float)nodeAnim->mScalingKeys[i + 1].mTime)
        {
            return i;
        }
    }

    throw 1;
}

uint JointTransform::findRotation(float ticks, const aiNodeAnim *nodeAnim)
{
    for (uint i = 0; i < nodeAnim->mNumRotationKeys - 1; i++)
    {
        if (ticks < (float)nodeAnim->mRotationKeys[i + 1].mTime)
        {
            return i;
        }
    }

    throw 1;
}
uint JointTransform::findPosition(float ticks, const aiNodeAnim *nodeAnim)
{
    for (uint i = 0; i < nodeAnim->mNumPositionKeys - 1; i++)
    {
        if (ticks < (float)nodeAnim->mPositionKeys[i + 1].mTime)
        {
            return i;
        }
    }

    throw 1;
}
