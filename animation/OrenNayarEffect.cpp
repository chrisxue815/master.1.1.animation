#include "OrenNayarEffect.h"

#include <gl/glfx.h>

#include <glm/gtc/type_ptr.hpp>

#include "Extensions.h"

using namespace chrisxue;
using namespace glm;

static const char *EffectFile = "contents/shaders/oren-nayar.glsl";

OrenNayarEffect::OrenNayarEffect(void) : Effect(EffectFile)
{
}

OrenNayarEffect::~OrenNayarEffect(void)
{
}

void OrenNayarEffect::init(void)
{
	compileProgram("OrenNayar");
	WVPLocation = getUniformLocation("WVP");
    WorldTransformLocation = getUniformLocation("WorldTransform");
    DirLightLocation.Color = getUniformLocation("DirectionalLight.Base.Color");
    DirLightLocation.AmbientIntensity = getUniformLocation("DirectionalLight.Base.AmbientIntensity");
    DirLightLocation.Direction = getUniformLocation("DirectionalLight.Direction");
    DirLightLocation.DiffuseIntensity = getUniformLocation("DirectionalLight.Base.DiffuseIntensity");
    CameraPositionLocation = getUniformLocation("CameraPosition");
    SpecularIntensityLocation = getUniformLocation("SpecularIntensity");
    SpecularPowerLocation = getUniformLocation("SpecularPower");
    AlbedoLocation = getUniformLocation("Albedo");
    RoughnessLocation = getUniformLocation("Roughness");
    TextureEnabledLocation = getUniformLocation("TextureEnabled");
    TextureLocation = getUniformLocation("Texture");

    use();

    DirectionalLight directionalLight;
    directionalLight.Color = vec3(1.0f, 1.0f, 1.0f);
    directionalLight.AmbientIntensity = 0.55f;
    directionalLight.DiffuseIntensity = 0.9f;
    directionalLight.Direction = vec3(-1.0f, -1.0f, -1.0f);
    setDirectionalLight(directionalLight);

    setSpecularIntensity(1);
    setSpecularPower(100);
    setAlbedo(1);
    setRoughness(0);

    setTextureEnabled(false);
    setTexture(0);

    unuse();
}

void OrenNayarEffect::setWVP(const mat4 &wvp)
{
    glUniformMatrix4fv(WVPLocation, 1, false, value_ptr(wvp));
}

void OrenNayarEffect::setWorldTransform(const mat4 &worldTransform)
{
    glUniformMatrix4fv(WorldTransformLocation, 1, false, value_ptr(worldTransform));
}

void OrenNayarEffect::setDirectionalLight(const DirectionalLight &light)
{
    glUniform3f(DirLightLocation.Color, light.Color.x, light.Color.y, light.Color.z);
    glUniform1f(DirLightLocation.AmbientIntensity, light.AmbientIntensity);
    vec3 Direction = light.Direction;
    Direction = safeNormalize(Direction);
    glUniform3f(DirLightLocation.Direction, Direction.x, Direction.y, Direction.z);
    glUniform1f(DirLightLocation.DiffuseIntensity, light.DiffuseIntensity);
}

void OrenNayarEffect::setCameraPosition(const glm::vec3 &cameraPosition)
{
    glUniform3f(CameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
}

void OrenNayarEffect::setSpecularIntensity(float intensity)
{
    glUniform1f(SpecularIntensityLocation, intensity);
}

void OrenNayarEffect::setSpecularPower(float power)
{
    glUniform1f(SpecularPowerLocation, power);
}

void OrenNayarEffect::setAlbedo(float albedo)
{
    glUniform1f(AlbedoLocation, albedo);
}

void OrenNayarEffect::setRoughness(float roughness)
{
    glUniform1f(RoughnessLocation, roughness);
}

void OrenNayarEffect::setTextureEnabled(bool textureEnabled)
{
    glUniform1i(TextureEnabledLocation, textureEnabled);
}

void OrenNayarEffect::setTexture(unsigned int texture)
{
    glUniform1i(TextureLocation, texture);
}
