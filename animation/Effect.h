#pragma once

#include <list>
#include <GL/glew.h>

namespace chrisxue
{
    class Effect
    {
    public:
        Effect(const char* pEffectFile);

        ~Effect();

        void use();
        void unuse();

    protected:
        void compileProgram(const char* program);

        GLint getUniformLocation(const char* uniformName);

        GLint getProgramParam(GLint param);

        static const int InvalidUniformLocation = -1;

    private:
        GLint effect;    
        GLint shaderProgram;
        const char* effectFile;
    };
}
