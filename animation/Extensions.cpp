#include "Extensions.h"

using namespace glm;

namespace chrisxue
{
    mat4 toGlmMat4(const aiMatrix4x4 &m)
    {
        mat4 p;
        p[0][0] = m.a1; p[1][0] = m.a2; p[2][0] = m.a3; p[3][0] = m.a4;
        p[0][1] = m.b1; p[1][1] = m.b2; p[2][1] = m.b3; p[3][1] = m.b4;
        p[0][2] = m.c1; p[1][2] = m.c2; p[2][2] = m.c3; p[3][2] = m.c4;
        p[0][3] = m.d1; p[1][3] = m.d2; p[2][3] = m.d3; p[3][3] = m.d4;
        //p[0][0] = m.a1; p[0][1] = m.a2; p[0][2] = m.a3; p[0][3] = m.a4;
        //p[1][0] = m.b1; p[1][1] = m.b2; p[1][2] = m.b3; p[1][3] = m.b4;
        //p[2][0] = m.c1; p[2][1] = m.c2; p[2][2] = m.c3; p[2][3] = m.c4;
        //p[3][0] = m.d1; p[3][1] = m.d2; p[3][2] = m.d3; p[3][3] = m.d4;
        return p;
    }

    mat4 toGlmMat4(const aiMatrix3x3 &m)
    {
        mat4 p;
        p[0][0] = m.a1; p[1][0] = m.a2; p[2][0] = m.a3; p[3][0] = 0;
        p[0][1] = m.b1; p[1][1] = m.b2; p[2][1] = m.b3; p[3][1] = 0;
        p[0][2] = m.c1; p[1][2] = m.c2; p[2][2] = m.c3; p[3][2] = 0;
        p[0][3] = 0;    p[1][3] = 0;    p[2][3] = 0;    p[3][3] = 1;
        //p[0][0] = m.a1; p[0][1] = m.a2; p[0][2] = m.a3; p[0][3] = 0;
        //p[1][0] = m.b1; p[1][1] = m.b2; p[1][2] = m.b3; p[1][3] = 0;
        //p[2][0] = m.c1; p[2][1] = m.c2; p[2][2] = m.c3; p[2][3] = 0;
        //p[3][0] = 0;    p[3][1] = 0;    p[3][2] = 0;    p[3][3] = 1;
        return p;
    }

    void print(const mat4 &m)
    {
        for (int row = 0; row < 4; row++)
        {
            for (int col = 0; col < 4; col++)
            {
                printf("%f ", m[col][row]);
            }
            printf("\n");
        }
    }

    quat rotate(const vec3 &source, const vec3 &Target)
    {
        float angle = acosD(dot(source, Target) / length(source) / length(Target));
        vec3 axis = safeNormalize(cross(source, Target));
        return angleAxis(angle, axis);
    }
}
