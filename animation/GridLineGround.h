#pragma once

#include "Entity.h"

#include <GL/glew.h>

namespace chrisxue
{
    class BasicEffect;

    class GridLineGround : public Entity
    {
    public:
        GridLineGround(void);
        ~GridLineGround(void);

        // override
        void init();
        void draw();

    private:
        static const int TileWidth = 10;
        static const int NumTilesPerEdge = 50;
        static const int Range = TileWidth * NumTilesPerEdge / 2;
        static const int NumLines = (NumTilesPerEdge + 1) * 2;
        static const int NumPoints = NumLines * 2;

        BasicEffect *effect;
        GLuint vao;
    };
}
