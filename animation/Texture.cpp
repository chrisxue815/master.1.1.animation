#include "Texture.h"

#include <iostream>

#include "Extensions.h"

using namespace chrisxue;
using namespace std;

map<string, Texture*> Texture::TextureMap;

Texture::Texture(GLenum textureTarget, const string &texturePath)
{
    this->textureTarget = textureTarget;
    this->texturePath = texturePath;
    image = NULL;

    try
    {
        image = new Magick::Image(texturePath);
        image->write(&blob, "RGBA");
    }
    catch (Magick::Error &error)
    {
        fprintf(stderr, "Error loading texture '%s': %s\n", texturePath, error.what());
        throw 1;
    }

    glGenTextures(1, &textureObj);
    glBindTexture(textureTarget, textureObj);
    glTexImage2D(textureTarget, 0, GL_RGBA, image->columns(), image->rows(), 0, GL_RGBA, GL_UNSIGNED_BYTE, blob.data());
    glTexParameterf(textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    if (glGetError() != GL_NO_ERROR)
    {
        throw 1;
    }
}

Texture *Texture::load(GLenum textureTarget, const string &texturePath)
{
    if (contains(TextureMap, texturePath))
    {
        return TextureMap[texturePath];
    }
    else
    {
        Texture *texture = new Texture(textureTarget, texturePath);
        TextureMap[texturePath] = texture;
        return texture;
    }
}

void Texture::bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(textureTarget, textureObj);
}
